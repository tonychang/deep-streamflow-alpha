# Deep Streamflow

Utilizing data from USGS gage stations, this project attempts to explore the possibility of using a deep learning based approach for computer vision to assess flow rate into three categories (Low, Med, High).

We use the keras package in Python to estimate flow from images using a 2d convolutional neural network and various architectures.

As of 3/12/2020, the model are performing well.

![accuracy](output/accuracy.png)
![confusion](output/conf_mat.png)
![validation](output/validation.png)